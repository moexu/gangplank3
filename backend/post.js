const mongoose = require('mongoose')

const Schema = mongoose.Schema

const post = new Schema(
  {
    title: String,
    date: Date,
    summary: String,
    progress: String,
    thoughts: String,
    linktext: String,
    url: String,
    image: String,
  }
)

module.exports = mongoose.model('Post', post)