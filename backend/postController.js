const mongoose = require('mongoose')
require('./post')
const Post = mongoose.model('Post')

exports.getAllPosts = function(req, res) {
  Post.find({}, function(err, result) {
    if (err) {
      res.send(err)
    } else {
      res.send(result)
    }
  })
}

exports.getPostPreviews = function(req, res) {
  Post.find({}, 'title date summary image', {sort: '-date'}, function(err, result) {
    if (err) {
      res.send(err)
    } else {
      res.send(result)
    }
  })
}

exports.getPost = function(req, res) {
  Post.findById(req.params.id, function(err, result) {
    if (err) {
      res.send(err)
    } else {
      res.send(result)
    }
  })
}
 
exports.addPost = async function(res, fields) {
  const post = new Post(fields)
  await post.save(function(err) {
    if (err) {
      res.send(err)
    } else {
      res.status(200).end()
    }
  })
}

exports.updatePost = async function(res, fields) {
  await Post.findByIdAndUpdate(fields._id, fields, function(err) { 
    if (err) {
      res.send(err)
    } else {
      res.status(200).end()
    }
  })
}

exports.deletePost = async function(req, res) {
  await Post.findByIdAndDelete(req.params.id, function(err) {
    if (err) {
      res.send(err)
    } else {
      res.status(200).end()
    }
  })
}

