require('dotenv').config()
const express = require('express')
const cors = require('cors')
const formidable = require('formidable')
const mongoose = require('mongoose') //.set('debug', true)
const router = express.Router()
const favicon = require('express-favicon')
const path = require('path')
const app = express()

app.use(favicon(path.join(__dirname + '/public/favicon.ico')))

const postController = require('./postController')

const port = process.env.PORT
const uri = 'mongodb+srv://' + process.env.DB_USER + ':' + process.env.DB_PW + '@' + process.env.DB_HOST + '/' + process.env.DB

mongoose.connect(uri, { useUnifiedTopology: true, useNewUrlParser: true })

const connection = mongoose.connection
connection.once('open', function() {
  //console.log("mongodb connection works")
})

app.use(cors())

app.use('/static', express.static(path.join(__dirname, 'public')))

app.use('/', router)

router.route('/').get(function(req, res) {
  res.sendFile(path.join(__dirname + '/public/index.html'))
})

router.route('/getAllPosts').get(function(req, res) {
  postController.getAllPosts(req, res)
})

router.route('/getPostPreviews').get(function(req, res) {
  postController.getPostPreviews(req, res)
})

router.route('/getPost/:id').get(function(req, res) {
  postController.getPost(req, res)
})

router.route('/addPost').post(function(req, res) {
  if (process.env.DEV == 'true') {
    const form = formidable()
    form.parse(req, (err, fields) => {
      postController.addPost(res, fields)
    })
  } else {
    res.status(403).sendFile(path.join(__dirname + '/public/no.html'))
  }
})

router.route('/updatePost').post(function(req, res) {
  if (process.env.DEV == 'true') {
    const form = formidable()
    form.parse(req, (err, fields) => {
      postController.updatePost(res, fields)
    })
  } else {
    res.status(403).sendFile(path.join(__dirname + '/public/no.html'))
  }
})

router.route('/deletePost/:id').post(function(req, res) {
  if (process.env.DEV == 'true') {
    postController.deletePost(req, res)
  } else {
    res.status(403).sendFile(path.join(__dirname + '/public/no.html'))
  }
})

router.route('/no').get(function(req, res) {
  res.sendFile(path.join(__dirname + '/public/no.html'))
})

app.use(function(req, res, next) {
  res.status(404).sendFile(path.join(__dirname + '/public/404.html'))
})

app.listen(port, function() {
  console.log("Server is running on port: " + port)
})