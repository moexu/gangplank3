import React from "react";
import "./App.scss";

import BlogForm from "./components/BlogForm";
import Footer from "./components/Footer";
import Header from "./components/Header";
import Menu from "./components/Menu";

const App = () => (
  <body>
    <Menu />
    <Header />
    <BlogForm />
    <Footer />
  </body>
)

export default App;
