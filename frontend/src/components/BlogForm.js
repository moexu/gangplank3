import React from "react";
import axios from "axios";

const initialState = {
  title: '',
  date: '',
  summary: '',
  progress: '',
  thoughts: '',
  linktext: '',
  url: '',
  image: ''
};

class BlogForm  extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);

    this.state = initialState;
  }
  
  handleSubmit = (event) => {
    axios({
      method: "post",
      url: "http://localhost:4000/addPost",
      data: this.state
    }).then((response) => {
      // TODO: actual error handling
      // also TODO: success notification
      this.setState(initialState);
      console.log("response", response);
    });
    event.preventDefault();
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  render() {
    return (
      <section className="section-form">
        <h1 className="section-title">Blog data entry</h1>
          <form onSubmit={this.handleSubmit}>
            <div className="form-wrapper-outer">
              <div className="form-wrapper-inner">

              <div className="row">
                <div className="form-group col-md-6">
                  <label htmlFor="title" className="form-label">Title</label>
                  <input 
                    type="text" 
                    name="title"
                    value={this.state.title}
                    onChange={this.handleInputChange} 
                    className="form-control" />
                </div>
                <div className="form-group col-md-6">
                  <label htmlFor="date" className="form-label">Date</label>
                  <input 
                    type="text" 
                    name="date"
                    value={this.state.date}
                    onChange={this.handleInputChange}
                    className="form-control" />
                </div>
              </div>

              <div className="row">&nbsp;</div>
              
              <div className="row">
                <div className="col-md-12">
                  <label htmlFor="summary" className="form-label">Summary</label>
                  <input 
                    type="text" 
                    name="summary" 
                    value={this.state.summary}
                    onChange={this.handleInputChange}
                    className="form-control" />
                </div>
              </div>

              <div className="row">&nbsp;</div>
              
              <div className="row">
                <div className="col-md-12">
                  <label htmlFor="progress" className="form-label">Progress</label>
                  <textarea 
                    name="progress" 
                    id="custom-textarea"
                    value={this.state.progress}
                    onChange={this.handleInputChange}
                    rows="5" 
                    className="form-control" />
                </div>
              </div>

              <div className="row">&nbsp;</div>
              
              <div className="row">
                <div className="col-md-12">
                  <label htmlFor="thoughts" className="form-label">Thoughts</label>
                  <textarea 
                    name="thoughts" 
                    id="custom-textarea"
                    value={this.state.thoughts}
                    onChange={this.handleInputChange} 
                    rows="5" 
                    className="form-control" />
                </div>
              </div>

              <div className="row">&nbsp;</div>
              
              <div className="row">
                <div className="col-md-12">
                  <label htmlFor="image" className="form-label">Image</label>
                  <input 
                    type="text" 
                    name="image"
                    value={this.state.image}
                    onChange={this.handleInputChange} 
                    className="form-control" />
                </div>
              </div>

              <div className="row">&nbsp;</div>
              
              <div class="row">
                <div className="form-group col-md-6">              
                  <label htmlFor="linktext" className="form-label">Link Text</label>
                  <input 
                    type="text" 
                    name="linktext"
                    value={this.state.linktext}
                    onChange={this.handleInputChange} 
                    className="form-control" />
                </div>
                <div className="form-group col-md-6">              
                  <label htmlFor="url" className="form-label">URL</label>
                  <input 
                    type="text" 
                    name="url"
                    value={this.state.url}
                    onChange={this.handleInputChange} 
                    className="form-control" />
                </div>
              </div>

              <div className="row">&nbsp;</div>
              
              <div className="row">
                <div className="col-md-12 text-center">
                  <button type="submit" className="btn-submit">Add to database</button>
                </div>
              </div>
            </div>
          </div>        
        </form>
    </section>
  )};

}

export default BlogForm;
