import React from "react";
import header_image from "../images/esports_keyboard.jpeg";

const Header = () => (
  <header className="header">
    <div className="header__image">
      <img src={header_image} alt="esports glowing multicolor keyboard" />
    </div>
  </header>
)

export default Header;